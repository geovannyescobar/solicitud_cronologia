﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="SolicitudCronologia.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Sistema de Solicitud de Cronologia</title>
    <link href="css/metro.css" rel="stylesheet" />
    <link href="css/metro-icons.css" rel="stylesheet" />
    <link href="css/metro-responsive.css" rel="stylesheet" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/metro.js"></script>
    <style>
        .login-form {
            width: 25rem;
            height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
    </style>
    <script>




        $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>

</head>
<body class="bg-grayLight">
    <div class="login-form padding20 block-shadow">
        <form runat="server" id="form_login" name="form_login">
            <h1 class="text-light">Indentificarse</h1>
            <hr class="thin"/>
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="user_login">Usuario:</label>
                <%--<input type="text" name="user_login" id="user_login">--%>
                <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <ASP:RequiredFieldValidator ControlToValidate="txtUser"
           Display="Static" ErrorMessage="Falta nombre de usuario" ForeColor="Red" runat="server" 
           ID="vUserName" />
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="user_password">Contraseña:</label>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <ASP:RequiredFieldValidator ControlToValidate="txtPassword"
          Display="Static" ErrorMessage="Falta Contraseña" runat="server" 
          ID="vUserPass" ForeColor="Red"/>
            <br />
            <asp:Label  ID="lbFallo" Text="Fallo inicio de seccion, revisar Usuario y Contraseña" CssClass="bg-red fg-white" runat="server"></asp:Label>
            <br />
            
            <div class="form-actions">               
                <asp:Button ID="btnEntrar" runat="server" CssClass="button success" OnClick="btnEntrar_Click" Text="Entrar" />
                <asp:Button ID="btnRegistrar"  CausesValidation="false" runat="server" CssClass="button link" Text="Registro" OnClick="btnRegistrar_Click" />
            </div>
            
        </form>
    </div>

</body>
</html>
