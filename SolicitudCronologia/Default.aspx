﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SolicitudCronologia.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Recorrido Patrullas</title>
    <meta charset="utf-8" />
    <link href="css/metro.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://192.168.1.119/librerias/3.21/esri/css/esri.css" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <script src="js/jquery-ui-timepicker-addon.js"></script>
    <script src="http://192.168.1.119/librerias/3.21/init.js"></script>
    <script type="text/javascript">

</script>
    <script type="text/javascript">
        $(function () {
            $.datepicker.regional['es'] = {
                closeText: 'Cerra',
                prevText: 'Atras',
                nextText: 'Siguiente',
                currentText: 'Ahora',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Аbril', 'Mayo', 'Junio',
                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Nomviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                    'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mir', 'Jue', 'Vier', 'Sab'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                weekHeader: 'Semana',
                dateFormat: 'dd-mm-yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);

            $.timepicker.regional['es'] = {
                timeOnlyTitle: 'Seleccione el Tiempo',
                timeText: 'Tiempo',
                hourText: 'Hora',
                minuteText: 'Minutos',
                secondText: 'Segundos',
                millisecText: 'Milesegundos',
                timezoneText: 'Zona de Horario',
                currentText: 'Hoy',
                closeText: 'Cerra',
                timeFormat: 'HH:mm:ss',
                amNames: ['AM', 'A'],
                pmNames: ['PM', 'P'],
                isRTL: false
            };
            $.timepicker.setDefaults($.timepicker.regional['es']);
            $("#txtFechaDocumento").datetimepicker(
                {
                    controlType: 'select',
                    oneLine: true,
                    timeFormat: 'HH:mm:ss',
                    dateFormat: "dd-mm-yy",
                }
            );
            $("#txtFechaR911").datetimepicker(
                {
                    controlType: 'select',
                    oneLine: true,
                    timeFormat: 'HH:mm:ss',
                    dateFormat: "dd-mm-yy",
                }
            );

        });
    </script>
    <style>
        #map {
            height: 520px;
            width: 100%;
        }

        .imagen {
            width: 100%;
            max-width: 700px;
            height: auto;
            margin-left: 30%;
        }
    </style>


</head>
<body>
    <form id="form1" name="form1" runat="server">

        <div class="flex-grid ">
            <div class="row margin10">
                <img src="imagen/se911.png" class="imagen" />
            </div>
            <div class="row">
                <div class="app-bar  darcula" data-role="appbar">
                    <a class="app-bar-element branding">DESIT</a>
                    <span class="app-bar-divider"></span>
                    <ul class="app-bar-menu">
                        <%--                        <li><a href="">Solicitar</a></li>
                        <li><a href="">Solicitantes</a></li>--%>
                    </ul>
                    <div class="app-bar-element place-right">
                        <span class="dropdown-toggle"><span class="mif-cog"></span><%Response.Write(Session["Nombre"]); %></span>
                        <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" data-no-close="true" style="width: 220px">
                            <h3 class="text-light">Opciones</h3>
                            <ul class="unstyled-list fg-dark">
                                <%--<li><a href="" class="fg-white1 fg-hover-yellow">Profile</a></li>
                            <li><a href="" class="fg-white2 fg-hover-yellow">Security</a></li>
                            <li><a href="" class="fg-white3 fg-hover-yellow">Salir</a></li>--%>
                                <asp:LinkButton CssClass="fg-white3 fg-hover-yellow" ID="LinkButton1" Text="Salir" runat="server" OnClick="LinkButton_Click" />
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="margin10">
            <h1>Solicitud Cronología</h1>
            <hr />
        </div>
        <div class=" margin20">
            <div class="grid" >
                <div class="row cells4">
                    <div class="cell">
                        <asp:Label ID="lbFechaDocumento" runat="server" Text="Fecha de documento:" AssociatedControlID="txtFechaDocumento"></asp:Label>
                        <asp:TextBox ID="txtFechaDocumento" CssClass="input-control text full-size" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtFechaDocumento" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Este campo es requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="cell">
                        <asp:Label ID="lbfechaR911" AssociatedControlID="txtFechaR911" Text="Fecha Respuesta 911" runat="server"></asp:Label>
                        <asp:TextBox ID="txtFechaR911" runat="server" CssClass="input-control text full-size"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtFechaR911" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Este campo es requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="cell">
                        <asp:Label ID="lbNumDodumento" runat="server" Text="Número de documento:" AssociatedControlID="txtNumDocumento"></asp:Label>
                        <asp:TextBox ID="txtNumDocumento" CssClass="input-control text full-size" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtNumDocumento" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Este campo es requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="cell">
                        <asp:Label ID="LbNomSolicitante" runat="server" Text="Nombre del solicitante" AssociatedControlID="cbnNomSolicitante"></asp:Label>
                        <asp:DropDownList ID="cbnNomSolicitante" runat="server" CssClass="input-control select full-size"></asp:DropDownList>
                    </div>
                </div>
                <div class="row cells4">
                    <div class="cell">
                        <asp:Label ID="LbDependencia" runat="server" Text="Dependencia" AssociatedControlID="cbnDependencias"></asp:Label>
                        <asp:DropDownList ID="cbnDependencias" runat="server" CssClass="input-control select full-size"></asp:DropDownList>
                    </div>
                    <div class="cell">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                        <asp:Label ID="LbDepartamento" runat="server" Text="Departamento" AssociatedControlID="cbnDepartamento"></asp:Label>
                        <asp:DropDownList CssClass="input-control select full-size" ID="cbnDepartamento" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cbnDepartamento_SelectedIndexChanged1">
                            <asp:ListItem Value="0" Text="Departamentos" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="VcbnDepartamento" ControlToValidate="cbnDepartamento" InitialValue="0" runat="server" ErrorMessage="Este campo es requerido" />
                    </div>
                    <div class="cell">
                        <asp:UpdatePanel ID="script1" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="LbMunicipio" runat="server" Text="Municipio" AssociatedControlID="cbnMunicipio"></asp:Label>
                                <asp:DropDownList ID="cbnMunicipio" runat="server" CssClass="input-control select full-size"></asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="cbnMunicipio" ID="RequiredFieldValidator6" ErrorMessage="Este campo es requerido" runat="server" ForeColor="Red"></asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="cell">
                        <asp:Label ID="LbPrioridad" runat="server" Text="Prioridad" AssociatedControlID="cbnPrioridad"></asp:Label>
                        <asp:DropDownList ID="cbnPrioridad" name="cbnPrioridad" runat="server" CssClass="input-control select full-size">
                            <asp:ListItem Text="NORMAL" Value="NORMAL" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="URGENTE" Value="URGENTE"></asp:ListItem>
                            <asp:ListItem Text="ESPECIAL" Value="ESPECIAL"></asp:ListItem>
                            <asp:ListItem Text="FLAGRANCIA" Value="FLAGRANCIA"></asp:ListItem>
                            <asp:ListItem Text="REO/TÉRMINO" Value="REO/TÉRMINO"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row cells4">
                    <div class="cell">
                        <asp:Label ID="lbTipo_Solicitud" runat="server" Text="Tipo Solicitud" AssociatedControlID="cbnTipo_Solicitud"></asp:Label>
                        <asp:DropDownList ID="cbnTipo_Solicitud" name="cbnTipo_Solicitud" runat="server" CssClass="input-control select full-size">
                            <asp:ListItem Text="LARGA" Value="LARGA"></asp:ListItem>
                            <asp:ListItem Text="CORTA" Value="CORTA"></asp:ListItem>
                            <asp:ListItem Text="GPS" Value="GPS "></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="cell">
                        <asp:Label ID="LbTipoCopia" runat="server" Text="Tipo Copia" AssociatedControlID="cbnTipoCopia"></asp:Label>
                        <asp:DropDownList ID="cbnTipoCopia" name="cbnTipoCopia" runat="server" CssClass="input-control select full-size">
                            <asp:ListItem Text="SIMPLE" Value="SIMPLE"></asp:ListItem>
                            <asp:ListItem Text="CERTIFICADA" Value="CERTIFICADA"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="cell">
                        <asp:Label ID="LbDescripcion" runat="server" Text="Descripción de la solicitud" AssociatedControlID="txtDescripcion"></asp:Label>
                        <asp:TextBox ID="txtDescripcion" TextMode="MultiLine" CssClass="input-control text full-size" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtDescripcion" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Este campo es requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="cell">
                        <asp:Label ID="Lbref" runat="server" Text="Referencia Fiscal" AssociatedControlID="txtRef"></asp:Label>
                        <asp:TextBox ID="txtRef" CssClass="input-control text full-size" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtRef" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Este campo es requerido" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row cells4">
                    <div class="cell colspan4" style="text-align: center;">
                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" class="button success" OnClick="btnEnviar_Click" />
                    </div>
                </div>
            </div>
        </div>

    </form>


</body>
</html>
