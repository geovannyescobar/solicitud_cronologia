﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registro.aspx.cs" Inherits="SolicitudCronologia.registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registro Usuario</title>
    <link href="css/metro.css" rel="stylesheet" />
    <link href="css/metro-icons.css" rel="stylesheet" />
    <link href="css/metro-responsive.css" rel="stylesheet" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/metro.js"></script>
    <script>
        function btnCancel_Click() {
            window.location = "login.aspx";
        }

    </script>
    <style>
        .imagen {
            width: 100%;
            max-width: 700px;
            height: auto;
            margin-left: 25%;
            margin-right: 30%
        }
    </style>
</head>
<body>


    <div class="grid ">
        <div class="row cells12 ">
            <div class="cell colspan12">
                <img src="imagen/se911.png" class="imagen" />
            </div>
        </div>
        <hr />
        <div class="row cells12">
            <div class="cell colspan12">
                <h1>Registro de Solicitante</h1>
            </div>
        </div>
    </div>

    <div class="container page-content">
        <form id="form1" runat="server">
            <div class="grid ">
                <div class="row cells3 ">
                    <div class="cell ">
                        <asp:Label AssociatedControlID="txtusuario" runat="server" Text="Usuario:"></asp:Label>
                        <asp:TextBox ID="txtusuario" runat="server" CssClass="input-control text full-size"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtusuario"
                            Display="Static" ErrorMessage="Falta nombre de usuario" ForeColor="Red" runat="server"
                            ID="vUserName" />
                    </div>
                    <div class="cell">
                        <asp:Label AssociatedControlID="txtcontraseña" runat="server" Text="Contraseña:"></asp:Label>
                        <asp:TextBox ID="txtcontraseña" runat="server" TextMode="Password" CssClass="input-control text full-size password"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtcontraseña"
                            Display="Static" ErrorMessage="Falta la Contraseña" ForeColor="Red" runat="server"
                            ID="vContra" />
                    </div>
                    <div class="cell">
                        <asp:Label AssociatedControlID="txtcontrarep" runat="server" Text="Repetir Contraseña:"></asp:Label>
                        <asp:TextBox ID="txtcontrarep" runat="server" TextMode="Password" CssClass="input-control text full-size password" OnTextChanged="txtcontrarep_TextChanged"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ForeColor="Red" ErrorMessage="Las Contraseñas no son iguales!!!" ControlToValidate="txtcontrarep" ControlToCompare="txtcontraseña"></asp:CompareValidator>
                    </div>
                </div>
                <div class="row cells3 margin10">
                    <div class="cell">
                        <asp:Label AssociatedControlID="txtNombre" runat="server" Text="Nombre Completo"></asp:Label>
                        <asp:TextBox ID="txtNombre" runat="server"  CssClass="input-control text full-size"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="txtNombre"
                            Display="Static" ErrorMessage="Falta Nombre Completo" ForeColor="Red" runat="server"
                            ID="RequiredFieldValidator1" />
                    </div>
                    <div class="cell">
                        <asp:Label AssociatedControlID="cbndependecia" runat="server" Text="Dependecia:"></asp:Label>
                        <asp:DropDownList ID="cbndependecia" CssClass="input-control select full-size" runat="server"></asp:DropDownList>
                    </div>
                    <div class="cell">
                        <asp:Label AssociatedControlID="cbngrado" runat="server" Text="Grado:"></asp:Label>
                        <asp:DropDownList ID="cbngrado" runat="server" CssClass="input-control select full-size"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="cell">
                    <asp:Button ID="btnregistrar" runat="server" OnClick="btnregistrar_Click" class="button success" Text="Registrar" />
                    <asp:Button ID="btnCancel" CausesValidation="false" runat="server" class="button primary" Text="Cancelar" OnClick="btnCancel_Click" />
                    <%--<button id="btnCancel" class="button primary" onclick="btnCancel_Click();">Cancelar</button>--%>
                </div>

            </div>
            <div>
            </div>
        </form>
    </div>
</body>
</html>
