﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudCronologia
{
    public class Utilidades
    {
        public void cargarCombo( DropDownList combobox, string Sql, string texto, string valor )
        {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString; ;
            con.Open();
            OleDbCommand comd = new OleDbCommand(Sql, con);
            OleDbDataReader sl = comd.ExecuteReader();
            combobox.DataSource = sl;
            combobox.DataTextField = texto;
            combobox.DataValueField = valor;
            combobox.DataBind();
            con.Close();
        }
    }
}