﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Configuration;

namespace SolicitudCronologia
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbFallo.Visible = false;
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString; 
            con.Open();
            string user = txtUser.Text;
            string pass = txtPassword.Text;
            string SQLval = "SELECT Nombre, usuario FROM registrados WHERE usuario ='" + user+ "' and password='" + pass+ "' ";
            OleDbCommand ur = new OleDbCommand(SQLval, con);
            OleDbDataReader login = ur.ExecuteReader();
            
            if (login.Read())
            {
                Session["Nombre"] = login.GetString(0);
                Session["usuario"] = login.GetString(1);
                
                Server.Transfer("Default.aspx");
            }
            else {
                lbFallo.Visible = true;
            }
            con.Close();
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("registro.aspx");
        }
    }
}