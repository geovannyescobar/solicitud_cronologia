﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;

namespace SolicitudCronologia
{
    public class Conexion
    {
        private SqlConnection SQLconexion;
        private String Cadena_ConexionDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString;
            }
        }

        public Conexion()
        {
            try
            {
                System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
                // conexion.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReConexion()
        {
            try
            {
                SQLconexion.Close();
                SQLconexion.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Close()
        {
            try
            {
                SQLconexion.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Open()
        {
            try
            {
                SQLconexion.Open();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet ExecSP(String nombre, string[] nombreparams, object[] contenidoparams)
        {
            try
            {
                SqlCommand command = new SqlCommand(nombre, SQLconexion);
                command.CommandType = CommandType.StoredProcedure;
                int index = 0;
                foreach (String n in nombreparams)
                {
                    command.Parameters.AddWithValue(n, contenidoparams[index]);
                    index++;
                }
                DbDataReader leido = command.ExecuteReader();
                DataSet retorno = new DataSet();
                retorno.Load(leido, LoadOption.OverwriteChanges, new String[] { "resultado" });
                return (retorno);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}