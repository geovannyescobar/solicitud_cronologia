﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudCronologia
{
    public partial class registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilidades tool = new Utilidades();
            tool.cargarCombo(cbndependecia, "SELECT NombreDepen FROM Dependencias", "NombreDepen", "NombreDepen");
            tool.cargarCombo(cbngrado, "SELECT Cargos FROM Parametros", "Cargos", "Cargos");
        }

        protected void btnregistrar_Click(object sender, EventArgs e)
        {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString;
            con.Open();
            string usuario = txtusuario.Text;
            string contra = txtcontraseña.Text;
            string depencia = cbndependecia.SelectedValue;
            string cargo = cbngrado.SelectedValue;
            string nombre = txtNombre.Text;
            string SQLCommand = "insert into [registrados](usuario, password, Dependecia, Grado, Nombre) values (@usuario, @contra, @depencia, @cargo, @Nombre)";
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = SQLCommand;
            cmd.Parameters.AddWithValue("@usuario", usuario);
            cmd.Parameters.AddWithValue("@contra", contra);
            cmd.Parameters.AddWithValue("@depencia", depencia);
            cmd.Parameters.AddWithValue("@cargo", cargo);
            cmd.Parameters.AddWithValue("@Nombre", nombre);
            cmd.Connection = con;
            int a = cmd.ExecuteNonQuery();
            ClientScript.RegisterStartupScript(GetType(), "MostrarMensaje", "alert(\"Usuario creado con exito!!!!\");", true);
            Response.Redirect("login.aspx");
        }

        protected void txtcontrarep_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("login.aspx");
           
        }
    }
}