﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolicitudCronologia
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilidades tool = new Utilidades();
            tool.cargarCombo(cbnNomSolicitante,"SELECT Cod_Solicitante, Nombre FROM Solicitantes", "Nombre", "Cod_Solicitante");
            tool.cargarCombo(cbnDependencias, "SELECT NombreDepen FROM Dependencias", "NombreDepen", "NombreDepen");
            if (!IsPostBack)
            {
                Departamentos();
            }
            if (Session["Usuario"] == null) {
                Response.Redirect("login.aspx");
            }

            
            

        }

        public void Departamentos() {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString; ;
            con.Open();
            string sqlsolictantes = "SELECT DISTINCT DEPARTAMENTO FROM Municipios WHERE(DEPARTAMENTO NOT IN('Región'))";
            OleDbCommand comd = new OleDbCommand(sqlsolictantes, con);
            OleDbDataReader sl = comd.ExecuteReader();
            cbnDepartamento.DataSource = sl;
            cbnDepartamento.DataTextField = "DEPARTAMENTO";
            cbnDepartamento.DataValueField = "DEPARTAMENTO";
            cbnDepartamento.DataBind();
            cbnDepartamento.Items.Insert(0, "Despartamentos");
            con.Close();
        }

        protected void cbnDepartamento_SelectedIndexChanged1(object sender, EventArgs e)
        {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString; ;
            con.Open();
            if (cbnDepartamento.SelectedIndex == 0)
            {
                cbnMunicipio.Items.Clear();
            }
            else {
                string sqlsolictantes = "SELECT MUNICIPIO FROM Municipios WHERE(DEPARTAMENTO = '" + cbnDepartamento.SelectedValue + "')";
                OleDbCommand comd = new OleDbCommand(sqlsolictantes, con);
                OleDbDataReader sl = comd.ExecuteReader();
                cbnMunicipio.DataSource = sl;
                cbnMunicipio.DataTextField = "MUNICIPIO";
                cbnMunicipio.DataValueField = "MUNICIPIO";
                cbnMunicipio.DataBind();
            }

            con.Close();
        }

        protected void LinkButton_Click(object sender, EventArgs e) {
            Session.Contents.RemoveAll();
            Session.Abandon();
            Response.Redirect("login.aspx");

        }


        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cadenaConexionSQL"].ConnectionString; 
            con.Open();
            int NuevoID;
            string id = null;
            int AñoActual = DateTime.Today.Year;
            string UltimoRegistro = "SELECT MAX(IdCaso) as Id FROM Recepcion_Solicitud WHERE AñoEnCurso = "+ AñoActual;

            OleDbCommand ur = new OleDbCommand(UltimoRegistro, con);
            id = ur.ExecuteScalar().ToString();
            if (id == "")
            {
                NuevoID = 1;
            }
            else {
                NuevoID = Int32.Parse(id) + 1;
            }
            string NCasoCalc = cbnTipo_Solicitud.SelectedValue.Substring(0, 1)+ DateTime.Today.Day+ DateTime.Today.Month+ DateTime.Today.Year.ToString().Substring(2,2)+"-"+ NuevoID;

            string Usuario = Session["Usuario"].ToString();
            string FechaActual = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string fechaOficio = txtFechaDocumento.Text;
            string No_Oficio = txtNumDocumento.Text;
            string Codigo_Solicitante = cbnNomSolicitante.SelectedValue;
            string Dependencia = cbnDependencias.SelectedValue;
            string Departamento = cbnDepartamento.SelectedValue;
            string Municipio = cbnMunicipio.SelectedValue;
            string FechaR911 = txtFechaR911.Text;
            string Prioridad = cbnPrioridad.SelectedValue;
            string Tipo_Solicitud = cbnTipo_Solicitud.SelectedValue;
            string Tipo_Copia = cbnTipoCopia.SelectedValue;
            string Descripcion_Solicitud = txtDescripcion.Text;
            string ReferenciaFiscal = txtRef.Text;

            string SQLCommand = "insert into [Recepcion_Solicitud] (IdCaso, AñoEnCurso, Operador_Ingreso, Fecha_Ingreso_Sistema, " +
            "Fecha_Oficio, No_Oficio,Cod_Solicitante, Dependencia, Departamento, Municipio, Fecha_R911, Prioridad, Tipo_Solicitud, Tipo_Copia, Descripcion_Solicitud, " +
            "Estado_solicitud, Operador_Inicio_Caso, Fecha_Inicio_Caso, ADDD, Numero_Caso, ConfCC, ReferenciaFiscal) values (@NuevoID, @AñoActual, @Usuario, @FechaActual, " +
            "@fechaOficio, @No_Oficio, @Codigo_Solicitante, @Dependencia, @Departamento, @Municipio, @FechaR911, @Prioridad, @Tipo_Solicitud, @Tipo_Copia, @Descripcion_Solicitud, " +
            "'REQ_INICIO', Null, Null, 1, @NCasoCalc, Null, @ReferenciaFiscal)";

            //string SQLCommand = "insert into [Solicitudes_online] (ONI, Cargo, Dependencia, Departamento, Municipios, ) ";

            try
            {
               OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = SQLCommand;
                cmd.Parameters.AddWithValue("@NuevoID", NuevoID);
                cmd.Parameters.AddWithValue("@AñoActual", AñoActual);
                cmd.Parameters.AddWithValue("@Usuario", Usuario);
                cmd.Parameters.AddWithValue("@FechaActual", FechaActual);
                cmd.Parameters.AddWithValue("@fechaOficio", fechaOficio);
                cmd.Parameters.AddWithValue("@No_Oficio", No_Oficio);
                cmd.Parameters.AddWithValue("@Codigo_Solicitante", Codigo_Solicitante);
                cmd.Parameters.AddWithValue("@Dependencia", Dependencia);
                cmd.Parameters.AddWithValue("@Departamento", Departamento);
                cmd.Parameters.AddWithValue("@Municipio", Municipio);
                cmd.Parameters.AddWithValue("@FechaR911", FechaR911);
                cmd.Parameters.AddWithValue("@Prioridad", Prioridad);
                cmd.Parameters.AddWithValue("@Tipo_Solicitud", Tipo_Solicitud);
                cmd.Parameters.AddWithValue("@Tipo_Copia", Tipo_Copia);
                cmd.Parameters.AddWithValue("@Descripcion_Solicitud", Descripcion_Solicitud);
                cmd.Parameters.AddWithValue("@NCasoCalc", NCasoCalc);
                cmd.Parameters.AddWithValue("@ReferenciaFiscal", ReferenciaFiscal);
                cmd.Connection = con;
                int a= cmd.ExecuteNonQuery();
                
                    ClientScript.RegisterStartupScript(GetType(), "MostrarMensaje", "$.Notify({keepOpen: true, type: 'success', caption: 'Ingreso Correcto', content: \"Su solicitud se aguardado correctamente y se le ha asignado el numero de caso "+ NCasoCalc + "\"});", true);
                    txtDescripcion.Text = "";
                    txtFechaDocumento.Text = "";
                    txtFechaR911.Text = "";
                    txtNumDocumento.Text = "";
                    txtRef.Text = "";
                    cbnDepartamento.SelectedIndex = 0;
                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            con.Close();
        }

        




    }
}